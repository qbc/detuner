﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckTunez
{
    public class HaarWavelet
    {
        public void DecomposeHaar(float[][] frames)
        {
            Decompose2d(frames);
        }

        private static void DecomposeArray(float[] array)
        {
            int len = array.Length;
            for (int i = 0; i < len; i++)
                array[i] /= (float)Math.Sqrt(len);

            float[] temp = new float[len];

            var sqrt2 = (float)Math.Sqrt(2);
            while (len > 1)
            {
                len /= 2;
                for (int i = 0; i < len; i++)
                {
                    var a = array[2 * i];
                    var b = array[2 * i + 1];
                    temp[i] = (a + b) / sqrt2;
                    temp[len + i] = (a - b) / sqrt2;
                }
                for (int i = 0; i < 2 * len; i++)
                    array[i] = temp[i];
            }
        }

        //private static void DecomposeArrayWithLibrary(float[] input, WaveletType type)
        //{
            //var wavelet = WaveletFactory.Create(type);
            //var doubleArray = Array.ConvertAll(input, x => (double)x);
            //var transformed = DwtTransform.Forward(doubleArray, wavelet, 2);
            //var fullVector = transformed.FullVector;
            //var floatArray = Array.ConvertAll(fullVector, x => (float)x);
            //for(int i=0; i<floatArray.Length; i++)
            //    input[i] = floatArray[i];
        //}

        private static void Decompose2d(float[][] image)
        {
            int rows = image.GetLength(0);
            int columns = image[0].Length;
            float[] column = new float[rows];

            //Decompose rows (128)
            for (int i = 0; i < rows; i++)
                DecomposeArray(image[i]);

            //Decompose columns (32)
            for (int col = 0; col < columns; col++)
            {
                for (int row = 0; row < rows; row++)
                    column[row] = image[row][col];

                DecomposeArray(column);
                
                for (int row = 0; row < rows; row++)
                    image[row][col] = column[row];             
            }
        }

        public bool[] ExtractTopWavelets(float[][] frames, int topWaveletsCount)
        {
            int rows = frames.GetLength(0);
            int cols = frames[0].Length;
            float[] concatenated = new float[rows * cols];

            for (int row = 0; row < rows; row++)
                Buffer.BlockCopy(frames[row], 0, concatenated, 
                    row * frames[row].Length * 4, frames[row].Length * 4);

            var indexes = Enumerable.Range(0, concatenated.Length).ToArray();
            Array.Sort(concatenated, indexes, new AbsComparator());
            return EncodeFingerprint(concatenated, indexes, topWaveletsCount);
        }

        internal class AbsComparator : IComparer<float>
        {
            public int Compare(float x, float y)
            {
                return Math.Abs(y).CompareTo(Math.Abs(x));
            }
        }

        public bool[] EncodeFingerprint(float[] concatenatedFingerprint, int[] sortedIndexes, int topWaveletsCount)
        {
            var result = new bool[concatenatedFingerprint.Length * 2];
            for (int i = 0; i < topWaveletsCount; i++)
            {
                var index = sortedIndexes[i];
                double value = concatenatedFingerprint[i];
                if (value > 0) // positive
                    result[index * 2] = true;
                else if (value < 0) // negative
                    result[(index * 2) + 1] = true;
            }
            return result;
        } 
    }
}
