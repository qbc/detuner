﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckTunez.Database.Entities
{
    public class HashtablesRow
    {
        // ID of track in database
        public int Track { get; set; }

        // Number of bucket that value belongs to
        public int BucketNumber { get; set; }

        // Value
        public long Value { get; set; }
    }
}
