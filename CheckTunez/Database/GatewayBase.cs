﻿using CheckTunez.Database.ConnectionFacade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckTunez.Database
{
    // Layer supertype

    public abstract class GatewayBase
    {
        protected readonly IDatabaseFacade _dbFacade;
        protected readonly string _dbName;

        public GatewayBase(IDatabaseFacade dbFacade, string dbName)
        {
            _dbFacade = dbFacade;
            _dbName = dbName;
        }

        public bool CheckTableExists()
        {
            if (!_dbFacade.IsConnectionOpened)
                _dbFacade.OpenConnection();

            var reader = _dbFacade.ExecuteQueryWithReader(string.Format("SELECT count(*) as cnt FROM sqlite_master WHERE type='table' AND name='{0}'", _dbName));
            if(reader != null && reader.Rows.Count>0)
                return (long)reader.Rows[0]["cnt"] == 1;
            reader.Dispose();

            _dbFacade.CloseConnection();
            return false;
        }

        public abstract void Create();

        /// <summary>
        /// Creates table if neccessary
        /// </summary>
        /// <returns>True if tables were already created</returns>
        public bool CreateIfNeccessary()
        {
            if (!CheckTableExists())
            {
                Create();
                return false;
            }
            return true;
        }
    }
}
