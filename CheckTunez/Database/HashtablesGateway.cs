﻿using CheckTunez.Database.ConnectionFacade;
using CheckTunez.Database.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckTunez.Database
{
    public class HashtablesGateway : GatewayBase
    {
        public static class Consts
        {
            public const string TableName = "Hashtables";

            public static class ColumnNames
            {
                public const string Track = "Track";
                public const string Bucket = "Bucket";
                public const string Value = "Value";
            }
        }

        public HashtablesGateway(IDatabaseFacade dbFacade): base(dbFacade, Consts.TableName)
        {
        }

        public override void Create()
        {
            string creationString = string.Format(
                "CREATE TABLE {0}({1} INTEGER NOT NULL, {2} INTEGER NOT NULL, {3} INTEGER NOT NULL, PRIMARY KEY({1}, {2}, {3}));",
                Consts.TableName,
                Consts.ColumnNames.Track,
                Consts.ColumnNames.Bucket,
                Consts.ColumnNames.Value);
            
            _dbFacade.ExecuteNonQuery(creationString);
        }

        public void InsertIfNotExists(HashtablesRow row)
        {
            var query = string.Format(
                "INSERT OR IGNORE INTO {0} ({1},{2},{3}) VALUES ({4},{5},{6})",
                Consts.TableName,
                Consts.ColumnNames.Track,
                Consts.ColumnNames.Bucket,
                Consts.ColumnNames.Value,
                row.Track,
                row.BucketNumber,
                row.Value);

            _dbFacade.ExecuteNonQuery(query);
        }

        public void Insert(HashtablesRow row)
        {
            var query = string.Format(
                "INSERT INTO {0} ({1},{2},{3}) VALUES ({4},{5},{6})",
                Consts.TableName,
                Consts.ColumnNames.Track,
                Consts.ColumnNames.Bucket,
                Consts.ColumnNames.Value,
                row.Track,
                row.BucketNumber,
                row.Value);

            _dbFacade.ExecuteNonQuery(query);
        }

        public List<HashtablesRow> DownloadHashtables()
        {
            var list = new List<HashtablesRow>();

            var query = string.Format(
                "SELECT {0}, {1}, {2} FROM {3}",
                Consts.ColumnNames.Track,
                Consts.ColumnNames.Bucket,
                Consts.ColumnNames.Value,
                Consts.TableName);

            var dt = _dbFacade.ExecuteQueryWithReader(query);

            if(dt!=null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    var hashTableRow = new HashtablesRow()
                    {
                        Track = (int)(long)row[Consts.ColumnNames.Track],
                        Value = (long)row[Consts.ColumnNames.Value],
                        BucketNumber = (int)(long)row[Consts.ColumnNames.Bucket]
                    };
                    list.Add(hashTableRow);
                }
            }

            return list;
        }

        internal bool Exists(HashtablesRow hashtablesRow)
        {
            var query = string.Format(
                "SELECT COUNT({0}) as cnt FROM {1} WHERE {0} = {2} AND {3} = {4} AND {5} = {6}",
                Consts.ColumnNames.Track,
                Consts.TableName,
                hashtablesRow.Track,
                Consts.ColumnNames.Bucket,
                hashtablesRow.BucketNumber,
                Consts.ColumnNames.Value,
                hashtablesRow.Value);

            var dt = _dbFacade.ExecuteQueryWithReader(query);
            if (dt!= null && dt.Rows.Count>0)
            {
                var exists = (long)dt.Rows[0]["cnt"] == 1;
                dt.Dispose();
                return exists;
            }
            return false;
        }

        internal void DeleteTrack(int id)
        {
            var query = string.Format(
                "DELETE FROM {0} WHERE {1} = {2}",
                Consts.TableName,
                Consts.ColumnNames.Track,
                id);

            _dbFacade.ExecuteNonQuery(query);
        }
    }
}
