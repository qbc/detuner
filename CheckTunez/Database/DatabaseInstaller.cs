﻿using CheckTunez.Database.ConnectionFacade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckTunez.Database
{
    public class DatabaseInstaller
    {
        private readonly IDatabaseFacade _dbFacade;

        public DatabaseInstaller(IDatabaseFacade connectionFacade)
        {
            _dbFacade = connectionFacade;
        }

        private bool CreateTablesIfNeccessary()
        {
            var tracks = new TrackGateway(_dbFacade);
            var tracksInstalled = tracks.CreateIfNeccessary();

            var config = new ConfigurationGateway(_dbFacade);
            var configInstalled = config.CreateIfNeccessary();

            var hashtables = new HashtablesGateway(_dbFacade);
            var hashtablesInstalled = hashtables.CreateIfNeccessary();

            return tracksInstalled && configInstalled && hashtablesInstalled;
        }

        public bool InstallIfNeccessary()
        {
            if(!_dbFacade.IsDbCreated())
                _dbFacade.CreateDb();

            return !CreateTablesIfNeccessary();
        }


    }
}
