﻿using CheckTunez.Database.ConnectionFacade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckTunez.Database
{
    public class ConfigurationMapper
    {
        private readonly ConfigurationGateway _configGateway;

        public ConfigurationMapper(ConfigurationGateway configGateway)
        {
            _configGateway = configGateway;
        }

        public int NumberOfHashtables
        {
            get { return int.Parse(_configGateway.GetParam(ConfigurationGateway.Consts.ParamNames.NumberOfHashtables)); }
            set { _configGateway.UpdateParam(ConfigurationGateway.Consts.ParamNames.NumberOfHashtables, value.ToString()); }
        }

        public int TrackDefinitionStepLength
        {
            get { return int.Parse(_configGateway.GetParam(ConfigurationGateway.Consts.ParamNames.TrackDefinitionStepLength)); }
            set { _configGateway.UpdateParam(ConfigurationGateway.Consts.ParamNames.TrackDefinitionStepLength, value.ToString()); }
        }

        public int TrackQueryMinStepLength
        {
            get { return int.Parse(_configGateway.GetParam(ConfigurationGateway.Consts.ParamNames.TrackQueryMinStepLength)); }
            set { _configGateway.UpdateParam(ConfigurationGateway.Consts.ParamNames.TrackQueryMinStepLength, value.ToString()); }
        }

        public int TrackQueryMaxStepLength
        {
            get { return int.Parse(_configGateway.GetParam(ConfigurationGateway.Consts.ParamNames.TrackQueryMaxStepLength)); }
            set { _configGateway.UpdateParam(ConfigurationGateway.Consts.ParamNames.TrackQueryMaxStepLength, value.ToString()); }
        }

        public int FingerprintSampleRate
        {
            get { return int.Parse(_configGateway.GetParam(ConfigurationGateway.Consts.ParamNames.FingerprintSampleRate)); }
            set { _configGateway.UpdateParam(ConfigurationGateway.Consts.ParamNames.FingerprintSampleRate, value.ToString()); }
        }

        public int OverlapOffsetSamples
        {
            get { return int.Parse(_configGateway.GetParam(ConfigurationGateway.Consts.ParamNames.OverlapOffsetSamples)); }
            set { _configGateway.UpdateParam(ConfigurationGateway.Consts.ParamNames.OverlapOffsetSamples, value.ToString()); }
        }

        public int WindowSamplesSize
        {
            get { return int.Parse(_configGateway.GetParam(ConfigurationGateway.Consts.ParamNames.WindowSamplesSize)); }
            set { _configGateway.UpdateParam(ConfigurationGateway.Consts.ParamNames.WindowSamplesSize, value.ToString()); }
        }

        public int MinFingerprintFrequency
        {
            get { return int.Parse(_configGateway.GetParam(ConfigurationGateway.Consts.ParamNames.MinFingerprintFrequency)); }
            set { _configGateway.UpdateParam(ConfigurationGateway.Consts.ParamNames.MinFingerprintFrequency, value.ToString()); }
        }

        public int MaxFingerprintFrequency
        {
            get { return int.Parse(_configGateway.GetParam(ConfigurationGateway.Consts.ParamNames.MaxFingerprintFrequency)); }
            set { _configGateway.UpdateParam(ConfigurationGateway.Consts.ParamNames.MaxFingerprintFrequency, value.ToString()); }
        }

        public int LogFrequenciesBase
        {
            get { return int.Parse(_configGateway.GetParam(ConfigurationGateway.Consts.ParamNames.LogFrequenciesBase)); }
            set { _configGateway.UpdateParam(ConfigurationGateway.Consts.ParamNames.LogFrequenciesBase, value.ToString()); }
        }

        public int LogFrequenciesBins
        {
            get { return int.Parse(_configGateway.GetParam(ConfigurationGateway.Consts.ParamNames.LogFrequenciesBins)); }
            set { _configGateway.UpdateParam(ConfigurationGateway.Consts.ParamNames.LogFrequenciesBins, value.ToString()); }
        }

        public int FingerprintImageLength
        {
            get { return int.Parse(_configGateway.GetParam(ConfigurationGateway.Consts.ParamNames.FingerprintImageLength)); }
            set { _configGateway.UpdateParam(ConfigurationGateway.Consts.ParamNames.FingerprintImageLength, value.ToString()); }
        }

        public int TopWavelets
        {
            get { return int.Parse(_configGateway.GetParam(ConfigurationGateway.Consts.ParamNames.TopWavelets)); }
            set { _configGateway.UpdateParam(ConfigurationGateway.Consts.ParamNames.TopWavelets, value.ToString()); }
        }

        public int FingerprintSamples
        {
            get { return FingerprintImageLength * OverlapOffsetSamples; }
        }

    }
}
