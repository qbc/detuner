﻿using CheckTunez.Database.Entities;
using CheckTunez.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckTunez.Database
{
    public class HashtablesMapper
    {
        private HashtablesGateway _hashTablesGateway;
        private TrackMapper _trackMapper;
        private ConfigurationMapper _configurationMapper;

        public HashtablesMapper(HashtablesGateway htGateway, TrackMapper trackMapper, ConfigurationMapper configurationMapper)
        {
            _hashTablesGateway = htGateway;
            _trackMapper = trackMapper;
            _configurationMapper = configurationMapper;
        }

        public void AddSingleTrackValues(Track track, long[] lshBucketValues)
        {
            if (lshBucketValues.Length != _configurationMapper.NumberOfHashtables)
                throw new Exception("Mismatch between LSH bucket count for track and in settings.");

            for (int bucketNumber = 0; bucketNumber < lshBucketValues.Length; bucketNumber++)
            {
                var hashtablesRow = new HashtablesRow()
                {
                    Track = track.Id,
                    BucketNumber = bucketNumber,
                    Value = lshBucketValues[bucketNumber]
                };
                 _hashTablesGateway.InsertIfNotExists(hashtablesRow);
            }
        }

        public Dictionary<long, HashSet<Track>>[] GetHashtables()
        {
            var hashTablesCount = _configurationMapper.NumberOfHashtables;
            var hashTablesApplicationStyle = new Dictionary<long, HashSet<Track>>[hashTablesCount];
            for(int i=0; i<hashTablesCount; i++)
                hashTablesApplicationStyle[i] = new Dictionary<long,HashSet<Track>>();

            var hashTablesDbStyle = _hashTablesGateway.DownloadHashtables();

            // row = track, value, bucketNumber.

            foreach(var row in hashTablesDbStyle)
            {
                var bucket = hashTablesApplicationStyle[row.BucketNumber];
                if (!bucket.ContainsKey(row.Value))
                    bucket[row.Value] = new HashSet<Track>();

                bucket[row.Value].Add(_trackMapper.GetTrack(row.Track));
            }

            return hashTablesApplicationStyle;
        }

    }
}
