﻿using CheckTunez.Database.ConnectionFacade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckTunez.Database
{
    public class ConfigurationGateway : GatewayBase
    {
        // Constants

        public static class Consts
        {
            public const string TableName = "Configuration";

            public static class ColumnNames
            {
                public const string Parameter = "Parameter";
                public const string Value = "Value";
            }

            public static class ParamNames
            {
                public const string NumberOfHashtables = "NumberOfHashtables";
                public const string TrackDefinitionStepLength = "TrackDefinitionStepLength";
                public const string TrackQueryMinStepLength = "TrackQueryMinStepLength";
                public const string TrackQueryMaxStepLength = "TrackQueryMaxStepLength";
                public const string FingerprintSampleRate = "FingerprintSampleRate";
                public const string OverlapOffsetSamples = "OverlapOffsetSamples";
                public const string WindowSamplesSize = "WindowSamplesSize";
                public const string MinFingerprintFrequency = "MinFingerprintFrequency";
                public const string MaxFingerprintFrequency = "MaxFingerprintFrequency";
                public const string LogFrequenciesBase = "LogFrequenciesBase";
                public const string LogFrequenciesBins = "LogFrequenciesBins";
                public const string FingerprintImageLength = "FingerprintImageLength";
                public const string TopWavelets = "TopWavelets";
            }
        }

        public static readonly Dictionary<string, string> Defaults = new Dictionary<string, string>()
        {
            {Consts.ParamNames.NumberOfHashtables, "25"},
            {Consts.ParamNames.TrackDefinitionStepLength, "5115"},
            {Consts.ParamNames.TrackQueryMinStepLength, "256"},
            {Consts.ParamNames.TrackQueryMaxStepLength, "512"},
            {Consts.ParamNames.FingerprintSampleRate, "5512"},
            {Consts.ParamNames.OverlapOffsetSamples, "64"},
            {Consts.ParamNames.WindowSamplesSize, "2048"},
            {Consts.ParamNames.MinFingerprintFrequency, "318"},
            {Consts.ParamNames.MaxFingerprintFrequency, "2000"},
            {Consts.ParamNames.LogFrequenciesBase, "2"},
            {Consts.ParamNames.LogFrequenciesBins, "32"},
            {Consts.ParamNames.FingerprintImageLength, "128"},
            {Consts.ParamNames.TopWavelets, "200"},
        };

        // Constructor

        public ConfigurationGateway(IDatabaseFacade dbFacade) : base(dbFacade, Consts.TableName)
        {
        }

        // Row editing methods

        private void InsertParam(string paramName, string value)
        {
            var query = string.Format("INSERT INTO {0} VALUES('{1}','{2}')", 
                Consts.TableName, paramName, value);
            _dbFacade.ExecuteNonQuery(query);
        }

        public string GetParam(string paramName)
        {
            var query = string.Format("SELECT {0} FROM {1} WHERE {2} = '{3}'",
                Consts.ColumnNames.Value, 
                Consts.TableName, 
                Consts.ColumnNames.Parameter, 
                paramName);

            var reader = _dbFacade.ExecuteQueryWithReader(query);

            if (reader == null || reader.Rows.Count == 0)
                return null;
            
            return (string)reader.Rows[0][0];
        }

        public void UpdateParam(string paramName, string value)
        {
            var query = string.Format("UPDATE {0} SET {1} = {2} WHERE {3} = '{4}'", 
                Consts.TableName, 
                Consts.ColumnNames.Value, 
                value, 
                Consts.ColumnNames.Parameter, 
                paramName);

            _dbFacade.ExecuteNonQuery(query);
        }

        public override void Create()
        {
            string creationString = string.Format("CREATE TABLE {0}({1} TEXT PRIMARY KEY, {2} TEXT NOT NULL);", 
                Consts.TableName, 
                Consts.ColumnNames.Parameter, 
                Consts.ColumnNames.Value);

            _dbFacade.ExecuteNonQuery(creationString);

            foreach(var key in Defaults.Keys)
                InsertParam(key, Defaults[key]);
 
        }
    }
}
