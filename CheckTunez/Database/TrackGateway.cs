﻿using CheckTunez.Database.ConnectionFacade;
using CheckTunez.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckTunez.Database
{
    public class TrackGateway : GatewayBase
    {
        public static class Consts
        {
            public const string TableName = "Tracks";

            public static class ColumnNames
            {
                public const string Id = "ID";
                public const string Artist = "Artist";
                public const string Title = "Title";
            }
        }

        public TrackGateway(IDatabaseFacade dbFacade) : base(dbFacade, Consts.TableName)
        {
        }

        public List<Track> GetAllTracks()
        {
            string query = string.Format(
                "Select {0}, {1}, {2} FROM {3}",
                Consts.ColumnNames.Id,
                Consts.ColumnNames.Artist,
                Consts.ColumnNames.Title,
                Consts.TableName);

            var tracks = new List<Track>();
            var dt = _dbFacade.ExecuteQueryWithReader(query);
            if(dt!=null)
            {
                foreach(DataRow row in dt.Rows)
                {
                    var track = new Track((int)(long)row[Consts.ColumnNames.Id])
                    {
                        Artist = (string)row[Consts.ColumnNames.Artist],
                        Title = (string)row[Consts.ColumnNames.Title]
                    };
                    tracks.Add(track);
                }
            }
            return tracks;
        }

        public void DeleteTrack(int id)
        {
            string query = string.Format(
                "DELETE FROM {0} WHERE {1} = {2}",
                Consts.TableName,
                Consts.ColumnNames.Id,
                id);

            _dbFacade.ExecuteNonQuery(query);
        }

        public void UpdateTrack(int id, string artist, string title)
        {
            string query = string.Format(
                "UPDATE {0} SET {1} = '{2}', {3} = '{4}' WHERE {5} = {6}",
                Consts.TableName,
                Consts.ColumnNames.Artist,
                artist.Replace("'", "''"),
                Consts.ColumnNames.Title,
                title.Replace("'", "''"),
                Consts.ColumnNames.Id,
                id);

            _dbFacade.ExecuteNonQuery(query);
        }

        public Track GetTrack(int id)
        {
            string query = string.Format(
                "Select {0}, {1}, {2} FROM {3} WHERE {0} = {4}",
                Consts.ColumnNames.Id,
                Consts.ColumnNames.Artist,
                Consts.ColumnNames.Title,
                Consts.TableName,
                id);

            var dt = _dbFacade.ExecuteQueryWithReader(query);
            if(dt!=null && dt.Rows.Count > 0)
            {
                return new Track(id)
                {
                    Artist = (string)dt.Rows[0][Consts.ColumnNames.Artist],
                    Title = (string)dt.Rows[0][Consts.ColumnNames.Title]
                };
            }
            return null;
        }

        public override void Create()
        {
            string creationString = string.Format(
                "CREATE TABLE {0}({1} INTEGER NOT NULL PRIMARY KEY, {2} TEXT NOT NULL, {3} TEXT NOT NULL);",
                Consts.TableName,
                Consts.ColumnNames.Id,
                Consts.ColumnNames.Artist,
                Consts.ColumnNames.Title);

            _dbFacade.ExecuteNonQuery(creationString);
        }


        internal Track AddTrack(string artist, string title)
        {
            string addString = string.Format(
                "INSERT INTO {0} ({1},{2}) VALUES ('{3}','{4}');SELECT last_insert_rowid();",
                Consts.TableName,
                Consts.ColumnNames.Artist,
                Consts.ColumnNames.Title,
                artist.Replace("'", "''"),
                title.Replace("'", "''"));

            var dt = _dbFacade.ExecuteQueryWithReader(addString);
            if(dt!=null && dt.Rows.Count>0)
            {
                var track = new Track((int)(long)dt.Rows[0][0])
                {
                    Artist = artist,
                    Title = title
                };
                dt.Dispose();
                return track;
            }

            return null;
        }
    }
}
