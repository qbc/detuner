﻿using CheckTunez.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckTunez.Database
{
    public class TrackMapper
    {
        private TrackGateway _trackGateway;
        private Dictionary<int, Track> _trackCache;

        public TrackMapper(TrackGateway trackGateway)
        {
            _trackGateway = trackGateway;
            _trackCache = new Dictionary<int, Track>();
        }

        public Track AddTrack(TrackName trackName)
        {
            var track = _trackGateway.AddTrack(trackName.Artist, trackName.Title);
            _trackCache.Add(track.Id, track);
            return track;
        }

        public List<Track> GetAllTracks()
        {
            var tracks = _trackGateway.GetAllTracks();
            foreach(var track in tracks)
            {
                if (!_trackCache.ContainsKey(track.Id))
                    _trackCache.Add(track.Id, track);
            }
            return tracks;
        }

        public List<Track> GetAllTracksFromCache()
        {
            return _trackCache.Select(t => t.Value).ToList();
        }

        public Track GetTrack(int id)
        {
            if (_trackCache.ContainsKey(id))
                return _trackCache[id];

            var track = _trackGateway.GetTrack(id);
            _trackCache.Add(id, track);
            return track;
        }


        public void Update(int id, string author, string name)
        {
            if(_trackCache.ContainsKey(id))
            {
                _trackCache[id].Artist = author;
                _trackCache[id].Title = name;
            }

            _trackGateway.UpdateTrack(id, author, name);
        }

        public void Delete(int id)
        {
            if (_trackCache.ContainsKey(id))
                _trackCache.Remove(id);

            _trackGateway.DeleteTrack(id);
        }
    }
}
