﻿using CheckTunez.Database.ConnectionFacade;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckTunez.Database
{
    class SqliteFacade : AbstractDatabaseFacade
    {
        private readonly string _dbFIleName;
        private string _connectionString;

        public SqliteFacade(string fileName = "Detuner.sqlite")
            : base()
        {
            _dbFIleName = fileName;
        }

        public override bool CreateDb()
        {
            SQLiteConnection.CreateFile("Detuner.sqlite");
            return true;
        }

        public override bool IsDbCreated()
        {
            return File.Exists(_dbFIleName);
        }

        public override bool OnOpenConnection()
        {
            if (!File.Exists(_dbFIleName))
                return false;

            _connectionString = string.Format("Data Source={0};Version=3;", _dbFIleName);
            return true;
        }

        public override bool OnCloseConnection()
        {
            return true;
        }

        public override int ExecuteNonQuery(string sql)
        {
            int result;
            using(var connection = new SQLiteConnection(_connectionString))
            {
                connection.Open();
                using (var command = new SQLiteCommand(sql, connection))
                {
                    result = command.ExecuteNonQuery();
                }
            }
            return result;
        }

        public override DataTable ExecuteQueryWithReader(string sql)
        {
            using(var connection = new SQLiteConnection(_connectionString))
            {
                connection.Open();
                using(var command = new SQLiteCommand(sql, connection))
                {
                    using(var reader = command.ExecuteReader())
                    {
                        var dataTable = new DataTable();
                        dataTable.Load(reader);
                        return dataTable;
                    }
                }
            }
        }

    }
}
