﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckTunez.Database.ConnectionFacade
{
    public interface IDatabaseFacade
    {
        bool IsConnectionOpened { get; }
        bool CreateDb();
        bool IsDbCreated();
        bool OpenConnection();
        bool CloseConnection();
        int ExecuteNonQuery(string sql);
        DataTable ExecuteQueryWithReader(string sql);
    }
}
