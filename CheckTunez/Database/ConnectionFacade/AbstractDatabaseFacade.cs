﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckTunez.Database.ConnectionFacade
{
    abstract class AbstractDatabaseFacade : IDatabaseFacade
    {
        private bool _isConnectonOpened = false;
        public bool IsConnectionOpened
        {
            get
            {
                return _isConnectonOpened;
            }
            private set 
            {
                _isConnectonOpened = value;
            }
        }

        public abstract bool CreateDb();
        public abstract bool IsDbCreated();
        public bool OpenConnection()
        {
            var success = OnOpenConnection();
            IsConnectionOpened = success;
            return success;
        }

        public bool CloseConnection()
        {
            var success = OnCloseConnection();
            IsConnectionOpened = !success;
            return success;
        }
        public abstract int ExecuteNonQuery(string sql);
        public abstract DataTable ExecuteQueryWithReader(string sql);

        public abstract bool OnOpenConnection();
        public abstract bool OnCloseConnection();
    }
}
