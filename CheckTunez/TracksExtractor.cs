﻿using CheckTunez.Database;
using CheckTunez.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckTunez
{
    class TracksExtractor
    {
        private HashtablesMapper _hashtablesMapper;

        public TracksExtractor(HashtablesMapper hashtablesMapper)
        {
            _hashtablesMapper = hashtablesMapper;
        }

        public Dictionary<Track, int> GetTracks(long[] fingerprint, int hashTableThreshold, int numberOfHashTables, Dictionary<long, HashSet<Track>>[] hashTables)
        {
            var result = new Dictionary<Track, int>();

            for (int i = 0; i < numberOfHashTables; i++)
            {
                if (hashTables[i].ContainsKey(fingerprint[i]))
                {
                    HashSet<Track> tracks = hashTables[i][fingerprint[i]]; 
                    foreach (Track track in tracks)
                    {
                        if (!result.ContainsKey(track))
                            result[track] = 1;
                        else
                            result[track]++;
                    }
                }
            }

            return result.Where(item => item.Value >= hashTableThreshold).ToDictionary(item => item.Key, item => item.Value);
        }  
    }
}
