﻿using CheckTunez.Controls;
using CheckTunez.Database;
using CheckTunez.FingerprintingSteps;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Un4seen.Bass;

namespace CheckTunez
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            BassNet.Registration("twojmail@gmail.com", "42345397456575");
            Bass.BASS_Init(-1, 44100, BASSInit.BASS_DEVICE_DEFAULT, IntPtr.Zero);

            var sqlFacade = new SqliteFacade();
            sqlFacade.OpenConnection();

            var installationManager = new DatabaseInstaller(sqlFacade);
            if (installationManager.InstallIfNeccessary())
                MessageBox.Show("Track database was not created or was corrupted.\nIt's structure has been repaired.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
            sqlFacade.CloseConnection();
        }

        private void SetContentControl(Control control)
        {
            control.Dock = DockStyle.Fill;
            panel1.Controls.Clear();
            panel1.Controls.Add(control);
        }

        private void btnAddToDb_Click(object sender, EventArgs e)
        {
            var ac = new AddControl();
            ac.SetController(new AddController(ac, new AddModel()));
            SetContentControl(ac);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            var sc = new SearchControl();
            sc.SetController(new SearchController(sc, new SearchModel()));
            SetContentControl(sc);
        }

        private void btnBrowseDb_Click(object sender, EventArgs e)
        {
            var dc = new DatabaseControl();
            dc.SetController(new DatabaseController(dc, new DatabaseModel()));
            SetContentControl(dc);
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            var sc = new SettingsControl();
            sc.SetController(new SettingsController(sc));
            SetContentControl(sc);
        }
    }
}
