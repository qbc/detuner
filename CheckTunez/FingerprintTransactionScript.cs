﻿using CheckTunez.Database;
using CheckTunez.Entities;
using CheckTunez.FingerprintingSteps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckTunez
{
    public class FingerprintTransactionScript
    {
        private ConfigurationMapper _config;
        private HashtablesMapper _hashTablesMapper;

        private MinHash _minHash = new MinHash();
        private LeastSensitiveHash _leastSensitiveHash = new LeastSensitiveHash();
        private IPermutations _permutations = new StaticPermutations();
        private FingerprintCreator _fingerprintCreator = new FingerprintCreator();
        private TrackNameResolver _trackNameResolver = new TrackNameResolver();
        private TracksExtractor _tracksExtractor;
        private TrackMapper _trackMapper;


        public FingerprintTransactionScript(ConfigurationMapper configurationMapper, 
            HashtablesMapper hashtablesMapper, TrackMapper trackMapper)
        {
            _config = configurationMapper;
            _hashTablesMapper = hashtablesMapper;
            _trackMapper = trackMapper;
            _tracksExtractor = new TracksExtractor(hashtablesMapper);
        }

        private List<long[]> GetBucketsForTrack(string fileName, IFingerprintingStep step, ConfigurationMapper config)
        {
            var fingerprints = _fingerprintCreator.CreateFingerprintsForFile(fileName, step, config).ToArray();
            var bucketsForAllFPs = new List<long[]>();            
            foreach (var fingerprint in fingerprints)
            {
                var minhash = _minHash.ComputeMinHashSignature(fingerprint, _permutations);
                var buckets = _leastSensitiveHash.GroupMinHashToLSHBuckets(minhash, config.NumberOfHashtables, 4);
                bucketsForAllFPs.Add(buckets);
            }
            return bucketsForAllFPs;
        }

        public List<Track> ExecuteSearch(string filename, Action<int> updateProgressDelegate)
        {
            updateProgressDelegate(0);

            var queryStep = new IncremantalRandomStep(_config.TrackQueryMinStepLength, 
                _config.TrackQueryMaxStepLength, _config.FingerprintSamples);

            var queryBuckets = GetBucketsForTrack(filename, queryStep, _config);

            updateProgressDelegate(20);

            var tracks = new Dictionary<Track,int>();
            var totalCandidateOccurences = 0;
            var pos = 0;

            var hashTables = _hashTablesMapper.GetHashtables();

            foreach(var fingerprint in queryBuckets)
            {
                var candidates = _tracksExtractor.GetTracks(fingerprint, 7, _config.NumberOfHashtables, hashTables);
                foreach(var candidate in candidates)
                {
                    if (!tracks.ContainsKey(candidate.Key))
                        tracks[candidate.Key] = 1;
                    else
                        tracks[candidate.Key]++;
                    totalCandidateOccurences++;
                }
                pos++;
                updateProgressDelegate(20 + 80 * pos / queryBuckets.Count);
            }

            foreach(var track in tracks.Keys)
                track.Probability = (double)tracks[track]/totalCandidateOccurences;

            var tracksWithProbabilities = tracks.Select(tr => tr.Key);
            return tracksWithProbabilities.OrderByDescending(x => x.Probability).ToList();
        }

        public void AddTrack(string filename)
        {
            var trackName = _trackNameResolver.GetTrackName(filename);
            var track = _trackMapper.AddTrack(trackName);

            var definitionStep = new IncrementalFixedStep(_config.TrackDefinitionStepLength, _config.FingerprintSamples);

            var queryBuckets = GetBucketsForTrack(filename, definitionStep, _config);

            foreach(var queryBucketsRow in queryBuckets)
                _hashTablesMapper.AddSingleTrackValues(track, queryBucketsRow);
        }
    }
}
