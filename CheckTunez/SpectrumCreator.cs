﻿using CheckTunez.Database;
using Exocortex.DSP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckTunez
{
    class SpectrumCreator
    {
        public float[][] CreateLogharitmicSpectrogram(float[] samples, ConfigurationMapper config)
        {
            var overlap = config.OverlapOffsetSamples;
            var windowSize = config.WindowSamplesSize;
            int framesNumber = (samples.Length - windowSize) / overlap;

            var frames = new List<float[]>();
            int[] fftBinIndexes = GenerateLogFrequenciesRanges(config);

            for (int i = 0; i < framesNumber; i++)
            {
                var fftSignal = new float[2 * windowSize];
                for (int j = 0; j < windowSize; j++)
                    fftSignal[2*j] = (samples[i*overlap+j]);

                Fourier.FFT(fftSignal, windowSize, FourierDirection.Forward);
                frames.Add(ExtractLogBins(fftSignal, fftBinIndexes));
            }
            return frames.ToArray();
        }


        private int[] GenerateLogFrequenciesRanges(ConfigurationMapper config)
        {
            var logMin = Math.Log(config.MinFingerprintFrequency, config.LogFrequenciesBase);
            var logMax = Math.Log(config.MaxFingerprintFrequency, config.LogFrequenciesBase);
            var delta = (logMax - logMin) / config.LogFrequenciesBins;

            int[] indexes = new int[config.LogFrequenciesBins + 1];
            double accDelta = 0;
            for (int i = 0; i <= config.LogFrequenciesBins; ++i)
            {
                var frequency = (float)Math.Pow(config.LogFrequenciesBase, logMin + accDelta);
                accDelta += delta;

                float fraction = frequency / ((float)config.FingerprintSampleRate / 2);
                indexes[i] = (int)Math.Round(((config.WindowSamplesSize / 2) + 1) * fraction);
            }

            return indexes;
        }

        private float[] ExtractLogBins(float[] spectrum, int[] logFrequenciesIndex)
        {
            int width = spectrum.Length / 2;
            var summedFrequencies = new float[32];
            for (int i = 0; i < 32; i++)
            {
                var lowerBound = logFrequenciesIndex[i];
                var upperBound = logFrequenciesIndex[i + 1];

                for (int k = lowerBound; k < upperBound; k++)
                {
                    double re = spectrum[2 * k] / ((float)width / 2);
                    double img = spectrum[(2 * k) + 1] / ((float)width / 2);
                    summedFrequencies[i] += (float)((re * re) + (img * img));
                }

                summedFrequencies[i] /= upperBound - lowerBound;
            }

            return summedFrequencies;
        }

    }
}
