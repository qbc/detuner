﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckTunez.FingerprintingSteps
{
    public class IncremantalRandomStep : IFingerprintingStep
    {
        private readonly int _samplesPerFingerprint;
        private readonly int _minStep;
        private readonly int _maxStep;
        private readonly int _firstStep;
        private readonly Random _random;

        public int FirstStep
        {
            get { return _firstStep; }
        }

        public int GetNextStep()
        {
            return -_samplesPerFingerprint + _random.Next(_minStep, _maxStep);
        }
         
        public IncremantalRandomStep(int minStep, int maxStep, int samplesPerFingerprint)
        {
            if (_minStep > _maxStep)
                throw new Exception("Minimal step should be smaller than maximal.");

            _minStep = minStep;
            _maxStep = maxStep;
            _samplesPerFingerprint = samplesPerFingerprint;
            _random = new Random(unchecked((int)DateTime.Now.Ticks));
            _firstStep = _random.Next(minStep, maxStep); 
        }

    }
}
