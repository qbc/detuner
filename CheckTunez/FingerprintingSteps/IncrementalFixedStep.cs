﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckTunez.FingerprintingSteps
{
    public class IncrementalFixedStep : IFingerprintingStep
    {
        private int _incrementBy;
        private int _nextStep;

        public int FirstStep
        {
            get { return 0; }
        }

        public int GetNextStep()
        {
            return _nextStep;
        }

        public IncrementalFixedStep(int incrementBy, int samplesPerFingerprint)
        {
            if (_incrementBy < 0)
                throw new ArgumentException("incrementBy value must be bigger or equal to 0");

            _incrementBy = incrementBy;
            _nextStep = -samplesPerFingerprint + incrementBy;
        }
    }
}
