﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckTunez
{
    interface IFingerprintingStep
    {
        int FirstStep { get; }
        int GetNextStep();
    }
}
