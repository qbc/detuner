﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckTunez
{
    public interface IPermutations
    {
        int[][] GetPermutations();
    }
}
