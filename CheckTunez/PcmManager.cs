﻿using CheckTunez.Database;
using Exocortex.DSP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Un4seen.Bass;
using Un4seen.Bass.AddOn.Mix;
using Un4seen.Bass.Misc;

namespace CheckTunez
{
    class PcmManager
    {
        private const int BufferSeconds = 10;
        private const float MinRms = 0.05F;
        private const float MaxRms = 3;

        private int CreateFileStream(string path)
        {
            int stream = Bass.BASS_StreamCreateFile(path,
                offset: 0,
                length: 0,
                flags: BASSFlag.BASS_STREAM_DECODE | BASSFlag.BASS_SAMPLE_MONO | BASSFlag.BASS_SAMPLE_FLOAT);

            if (stream == 0)
                throw new Exception(String.Format("Failed to create stream for file {0} - BASS library error code {1}", path, Bass.BASS_ErrorGetCode().ToString()));

            return stream;
        }

        private int CreateMixerStream(int sampleRate)
        {
            int stream = BassMix.BASS_Mixer_StreamCreate(sampleRate, 
                chans: 1,
                flags: BASSFlag.BASS_STREAM_DECODE | BASSFlag.BASS_SAMPLE_MONO | BASSFlag.BASS_SAMPLE_FLOAT);

            if (stream == 0)
                throw new Exception(String.Format("Failed to create mixer stream - BASS library error code {0}", Bass.BASS_ErrorGetCode().ToString()));

            return stream;
        }

        public float[] GetMonoSamples(string path, int sampleRate)
        {
            var fileStream = CreateFileStream(path);
            var mixerStream = CreateMixerStream(sampleRate);

            var channelAdded = BassMix.BASS_Mixer_StreamAddChannel(mixerStream, fileStream, flags: 0);
            if(!channelAdded)
                throw new Exception(String.Format("Unable to add channel to mixer stream. BASS error code: {0}", Bass.BASS_ErrorGetCode().ToString()));

            var bufferSize = sampleRate * BufferSeconds * sizeof(float);
            var buffer = new float[bufferSize];
            var chunks = new List<float[]>();

            int monoBytesRead = 0;
            while ((monoBytesRead = Bass.BASS_ChannelGetData(mixerStream, buffer, bufferSize)) > 0)
            {
                var monoFloatsRead = monoBytesRead / sizeof(float);
                var chunk = new float[monoFloatsRead];
                Array.Copy(buffer, chunk, monoFloatsRead);
                chunks.Add(chunk);
            }

            return chunks.SelectMany(chunk => chunk).ToArray();
        }

        public void SaveMonoSamplesToFile(float[] samples, string path)
        {
            WaveWriter waveWriter = new WaveWriter(path, 1, 5512, 16, true);

            const int bufferSize = 32768;
            int pos = 0;
            float[] buffer = new float[bufferSize];
            while (pos<samples.Length)
            {
                var left = samples.Length - pos;
                if (left < bufferSize)
                {
                    Array.Copy(samples, pos, buffer, 0, left);
                    waveWriter.Write(buffer, left*sizeof(float));
                    break;
                }
                Array.Copy(samples, pos, buffer, 0, bufferSize);
                waveWriter.Write(buffer, bufferSize*sizeof(float));
                pos += bufferSize;
            }

            waveWriter.Close();
        }

        public void NormalizePcmTable(float[] samples)
        {
            double squareSum = samples.Select(x => x * x).Sum();
            float rootMeanSquare = (float)Math.Sqrt(squareSum / samples.Length) * 10;

            if (rootMeanSquare < MinRms)
                rootMeanSquare = MinRms;
            else if (rootMeanSquare > MaxRms)
                rootMeanSquare = MaxRms;

            for (int i = 0; i < samples.Length; i++)
                samples[i] = Math.Max(Math.Min(samples[i] / rootMeanSquare, 1), -1);
        }

    }
}
