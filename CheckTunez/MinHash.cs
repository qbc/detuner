﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckTunez
{
    public class MinHash
    {
        public int[] ComputeMinHashSignature(bool[] fingerprint, IPermutations permutations)
        {
            var permutationsArray = permutations.GetPermutations();
            var signature = (bool[])fingerprint.Clone();

            var minHash = new List<int>();
            foreach(var permutation in permutationsArray)
            {
                var pos = permutation.Length;
                for(int j=0; j<permutation.Length; j++)
                {
                    if(signature[permutation[j]])
                    {
                        pos = j;
                        break;
                    }
                }
                minHash.Add(pos);
            }
            return minHash.ToArray(); 
        }
    }
}
