﻿using CheckTunez.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckTunez.Controls
{
    class SettingsController
    {
        private SettingsControl _control;
        private ConfigurationGateway _configurationGateway; // Model ;)

        public SettingsController(SettingsControl control)
        {
            _control = control;

            var dbFacade = new SqliteFacade();
            dbFacade.OpenConnection();
            _configurationGateway = new ConfigurationGateway(dbFacade);
        }

        public void LoadSettings()
        {
            foreach (var key in ConfigurationGateway.Defaults.Keys)
            {
                var value = _configurationGateway.GetParam(key);
                _control.SetControlValueForParameter(key, value);
            }
        }

        internal void Save()
        {
            foreach (var key in ConfigurationGateway.Defaults.Keys)
            {
                var value = _control.GetControlValueForParameter(key);
                _configurationGateway.UpdateParam(key, value);
            }
        }

        internal void Reset()
        {
            foreach(var key in ConfigurationGateway.Defaults.Keys)
            {
                _control.SetControlValueForParameter(key, ConfigurationGateway.Defaults[key]);
            }
        }
    }
}
