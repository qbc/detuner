﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CheckTunez.Database;

namespace CheckTunez.Controls
{
    public partial class SettingsControl : UserControl
    {
        private SettingsController _controller;

        public SettingsControl()
        {
            InitializeComponent();
        }

        internal void SetController(SettingsController settingsController)
        {
            _controller = settingsController;
            _controller.LoadSettings();
        }

        private TextBox GetTextBoxForParameter(string parameter)
        {
            switch (parameter)
            {
                case ConfigurationGateway.Consts.ParamNames.NumberOfHashtables:
                    return txtNumberOfHashtables;
                case ConfigurationGateway.Consts.ParamNames.TrackDefinitionStepLength:
                    return txtTrackDefinitionStepLength;
                case ConfigurationGateway.Consts.ParamNames.TrackQueryMinStepLength:
                    return txtTrackQueryMinStepLength;
                case ConfigurationGateway.Consts.ParamNames.TrackQueryMaxStepLength:
                    return txtTrackQueryMaxStepLength;
                case ConfigurationGateway.Consts.ParamNames.FingerprintSampleRate:
                    return txtFingerprintSampleRate;
                case ConfigurationGateway.Consts.ParamNames.OverlapOffsetSamples:
                    return txtOverlapOffsetSamples;
                case ConfigurationGateway.Consts.ParamNames.WindowSamplesSize:
                    return txtWindowSamplesSize;
                case ConfigurationGateway.Consts.ParamNames.MinFingerprintFrequency:
                    return txtMinFingerprintFrequency;
                case ConfigurationGateway.Consts.ParamNames.MaxFingerprintFrequency:
                    return txtMaxFingerprintFrequency;
                case ConfigurationGateway.Consts.ParamNames.LogFrequenciesBase:
                    return txtLogFrequenciesBase;
                case ConfigurationGateway.Consts.ParamNames.LogFrequenciesBins:
                    return txtLogFrequenciesBins;
                case ConfigurationGateway.Consts.ParamNames.FingerprintImageLength:
                    return txtFingerprintImageLength;
                case ConfigurationGateway.Consts.ParamNames.TopWavelets:
                    return txtTopWavelets;
                default:
                    return null;
            }
        }

        public string GetControlValueForParameter(string parameter)
        {
            var tb = GetTextBoxForParameter(parameter);
            return tb.Text;
        }

        public void SetControlValueForParameter(string parameter, string value)
        {
            var tb = GetTextBoxForParameter(parameter);
            tb.Text = value;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            _controller.Save();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            _controller.Reset();
        }
    }
}
