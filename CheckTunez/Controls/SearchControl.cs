﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CheckTunez.Entities;

namespace CheckTunez.Controls
{
    public partial class SearchControl : UserControl
    {
        private SearchController _controller;

        public SearchControl()
        {
            InitializeComponent();
        }

        public void SetController(SearchController controller)
        {
            _controller = controller;
        }

        public void UpdateFile(string newFile)
        {
            textBox1.Text = newFile;
        }

        public string GetSearchedFile()
        {
            return textBox1.Text;
        }

        public void SetInputEnabled(bool enabled)
        {
            textBox1.Enabled = enabled;
            btnChooseFile.Enabled = enabled;
            btnStart.Enabled = enabled;
        }

        public void SetProgressLabel(string text)
        {
            lblProgressText.Text = text;
        }

        public void SetProgressBar(int percentage)
        {
            progressBar1.Value = percentage;
        }

        public void FeedTableWithTracks(List<Track> tracks)
        {
            dataGridView1.DataSource = tracks;
            dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridView1.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView1.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridView1.AutoSize = true;
            dataGridView1.AutoResizeColumns();
        }

        private void btnChooseFile_Click(object sender, EventArgs e)
        {
            _controller.ChooseFile();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            _controller.PerformSearch();
        }
    }
}
