﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CheckTunez.Entities;

namespace CheckTunez.Controls
{
    public partial class DatabaseControl : UserControl
    {
        private DatabaseController _controller;
        private DataGridViewButtonColumn _updateColumn;
        private DataGridViewButtonColumn _deleteColumn;

        public DatabaseControl()
        {
            InitializeComponent();

            dataGridView1.CellContentClick += dataGridView1_CellContentClick;

            _updateColumn = new DataGridViewButtonColumn();
            _updateColumn.Name = _updateColumn.Text = "Update";
            _updateColumn.UseColumnTextForButtonValue = true;
            _updateColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            _deleteColumn = new DataGridViewButtonColumn();
            _deleteColumn.Name = _deleteColumn.Text = "Delete";
            _deleteColumn.UseColumnTextForButtonValue = true;
            _deleteColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
        }

        public void SetController(DatabaseController databaseController)
        {
            _controller = databaseController;
            _controller.Initialize();
        }

        internal void FeedTableWithTracks(List<Track> tracks)
        {
            dataGridView1.DataSource = tracks;

            dataGridView1.Columns["Id"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridView1.Columns["Id"].ReadOnly = true;            
            dataGridView1.Columns["Artist"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;            
            dataGridView1.Columns["Title"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;            
            dataGridView1.Columns["Probability"].Visible = false;

            if(!dataGridView1.Columns.Contains(_updateColumn))
                dataGridView1.Columns.Add(_updateColumn);

            if (!dataGridView1.Columns.Contains(_deleteColumn))
                dataGridView1.Columns.Add(_deleteColumn);
            
            dataGridView1.AutoSize = true;
            dataGridView1.AutoResizeColumns();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                if (senderGrid.Columns[e.ColumnIndex].Name == "Update")
                    _controller.UpdateRow(e.RowIndex);
                if (senderGrid.Columns[e.ColumnIndex].Name == "Delete")
                    _controller.DeleteRow(e.RowIndex);
            }
        }


        internal int GetTrackIdForRow(int rowNumber)
        {
            return (int)dataGridView1.Rows[rowNumber].Cells[2].Value;
        }

        internal string GetTrackAuthorForRow(int rowNumber)
        {
            return (string)dataGridView1.Rows[rowNumber].Cells[3].Value;
        }
        
        internal string GetTrackNameForRow(int rowNumber)
        {
            return (string)dataGridView1.Rows[rowNumber].Cells[4].Value;
        }
    }
}
