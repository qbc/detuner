﻿using CheckTunez.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CheckTunez.Controls
{
    public class AddController
    {
        private AddControl _control;
        private AddModel _model;
        private BackgroundWorker _bgWorker;

        public AddController(AddControl control, AddModel model)
        {
            _control = control;
            _model = model;

            _bgWorker = new BackgroundWorker();
            _bgWorker.DoWork += bgWorker_DoWork;
            _bgWorker.ProgressChanged += bgWorker_ProgressChanged;
            _bgWorker.WorkerSupportsCancellation = true;
            _bgWorker.WorkerReportsProgress = true;
        }

        public void ChooseFile()
        {
            using (var dialog = new OpenFileDialog())
            {
                dialog.Filter = "MPEG Layer-3 Audio files (*.mp3)|*.mp3";
                var result = dialog.ShowDialog();
                if (result == DialogResult.OK)
                    _control.UpdateSelectedPath(dialog.FileName);
            }
        }

        public void ChooseFolder()
        {
            using (var dialog = new FolderBrowserDialog())
            {
                var result = dialog.ShowDialog();
                if (result == DialogResult.OK)
                    _control.UpdateSelectedPath(dialog.SelectedPath);
            }
        }

        public void AddPath()
        {
            var addedPath = _control.GetPath();

            var isFile = File.Exists(addedPath);
            var isFolder = isFile ? false : Directory.Exists(addedPath);

            List<TrackToAdd> tracks;
            if (isFile)
            {
                tracks = new List<TrackToAdd>() { _model.PrepareSingleTrackToAdd(addedPath) };
            }
            else if (isFolder)
            {
                tracks = _model.PrepareFolderOFTracksToAdd(addedPath);
            }
            else
            {
                tracks = new List<TrackToAdd>();
            }

            _control.FeedTableWithTracksToAdd(tracks);

            _bgWorker.RunWorkerAsync(tracks);
        }

        public void StopProcessing()
        {
            if (_bgWorker.IsBusy)
                _bgWorker.CancelAsync();
        }

        private void bgWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            _control.UpdateProgressBar(e.ProgressPercentage);
            _control.RefreshGrid();
        }

        private void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            _control.SetInputEnabled(false);   
            var tracks = (List<TrackToAdd>)e.Argument;
            var i = 0;
            foreach(var track in tracks)
            {
                _bgWorker.ReportProgress(i *100/ tracks.Count);
                _model.AddTrackToDatabase(track);
                if (_bgWorker.CancellationPending)
                    break;
                track.Added = true;
                Thread.Sleep(100);
                i++;
            }
            _bgWorker.ReportProgress(100);
            _control.SetInputEnabled(true);
        }
    }
}
