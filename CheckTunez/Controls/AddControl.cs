﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CheckTunez.Entities;

namespace CheckTunez.Controls
{
    public partial class AddControl : UserControl
    {
        private AddController _addController;

        public AddControl()
        {
            InitializeComponent();
        }

        public void SetController(AddController addController)
        {
            _addController = addController;
        }

        public string GetPath()
        {
            return textBox1.Text;
        }

        public void UpdateSelectedPath(string path)
        {
            textBox1.Text = path;
        }

        public void UpdateProgressBar(int percents)
        {
            progressBar1.Value = percents;
        }

        private void SetEnabledWithInvoke(Control control, bool enabled)
        {
            if (control.InvokeRequired)
                control.Invoke(new Action(() => control.Enabled = enabled));
            else
                control.Enabled = enabled;
        }


        public void SetInputEnabled(bool enabled)
        {
            SetEnabledWithInvoke(btnAdd, enabled);
            SetEnabledWithInvoke(btnFolder, enabled);
            SetEnabledWithInvoke(btnFile, enabled);
            SetEnabledWithInvoke(textBox1, enabled);
            SetEnabledWithInvoke(btnStop, !enabled);
        }

        public void FeedTableWithTracksToAdd(List<TrackToAdd> tracks)
        {
            dataGridView1.DataSource = tracks;
            dataGridView1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridView1.AutoSize = true;
            dataGridView1.AutoResizeColumns();

        }

        public void RefreshGrid()
        {
            dataGridView1.Refresh();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnFile_Click(object sender, EventArgs e)
        {
            _addController.ChooseFile();
        }

        private void btnFolder_Click(object sender, EventArgs e)
        {
            _addController.ChooseFolder();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            _addController.AddPath();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            _addController.StopProcessing();
        }
    }
}
