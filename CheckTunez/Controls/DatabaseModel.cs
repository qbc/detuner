﻿using CheckTunez.Database;
using CheckTunez.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckTunez.Controls
{
    public class DatabaseModel
    {
        private TrackMapper _trackMapper;
        private HashtablesGateway _hashTablesGateway;
        private SqliteFacade _sqlFacade;

        public DatabaseModel()
        {
            _sqlFacade = new SqliteFacade();
            _trackMapper = new TrackMapper(new TrackGateway(_sqlFacade));
            _hashTablesGateway = new HashtablesGateway(_sqlFacade);
        }

        public List<Track> GetTracks()
        {
            _sqlFacade.OpenConnection();
            var tracks = _trackMapper.GetAllTracks();
            _sqlFacade.CloseConnection();
            return tracks;
        }

        public List<Track> GetTracksFromCache()
        {
            return _trackMapper.GetAllTracksFromCache();
        }

        public void UpdateTrack(int id, string author, string name)
        {
            _sqlFacade.OpenConnection();
            _trackMapper.Update(id, author, name);
            _sqlFacade.CloseConnection();
        }

        public void DeleteTrack(int id)
        {
            _sqlFacade.OpenConnection();
            _trackMapper.Delete(id);
            _hashTablesGateway.DeleteTrack(id);
            _sqlFacade.CloseConnection();
        }
    }
}
