﻿namespace CheckTunez.Controls
{
    partial class SettingsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.btnReset = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtNumberOfHashtables = new System.Windows.Forms.TextBox();
            this.txtTrackDefinitionStepLength = new System.Windows.Forms.TextBox();
            this.txtTrackQueryMinStepLength = new System.Windows.Forms.TextBox();
            this.txtTrackQueryMaxStepLength = new System.Windows.Forms.TextBox();
            this.txtFingerprintSampleRate = new System.Windows.Forms.TextBox();
            this.txtOverlapOffsetSamples = new System.Windows.Forms.TextBox();
            this.txtWindowSamplesSize = new System.Windows.Forms.TextBox();
            this.txtMinFingerprintFrequency = new System.Windows.Forms.TextBox();
            this.txtMaxFingerprintFrequency = new System.Windows.Forms.TextBox();
            this.txtLogFrequenciesBase = new System.Windows.Forms.TextBox();
            this.txtLogFrequenciesBins = new System.Windows.Forms.TextBox();
            this.txtFingerprintImageLength = new System.Windows.Forms.TextBox();
            this.txtTopWavelets = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.label11, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.label12, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.label13, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.label14, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 14);
            this.tableLayoutPanel1.Controls.Add(this.txtNumberOfHashtables, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtTrackDefinitionStepLength, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtTrackQueryMinStepLength, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtTrackQueryMaxStepLength, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.txtFingerprintSampleRate, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.txtOverlapOffsetSamples, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.txtWindowSamplesSize, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.txtMinFingerprintFrequency, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.txtMaxFingerprintFrequency, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.txtLogFrequenciesBase, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.txtLogFrequenciesBins, 1, 11);
            this.tableLayoutPanel1.Controls.Add(this.txtFingerprintImageLength, 1, 12);
            this.tableLayoutPanel1.Controls.Add(this.txtTopWavelets, 1, 13);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 16;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(603, 385);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label1, 2);
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(597, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "NOTE: Modifying settings below can cause program to malfunction. Changes should b" +
    "e done only with proper knowledge.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Number of hashtables";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 39);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(250, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Step length for creating fingerprint of track definition";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 65);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(187, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Minimal step for fingerprint for querying";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 91);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(190, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Maximal step for fingerprint for querying";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 117);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(141, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Sample rate for fingerprinting";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 143);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(165, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Overlapping samples in fingerprint";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 169);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(168, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Size of fingerprint window samples";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 195);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(141, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Minimal fingerprint frequency";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 221);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(144, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "Maximal fingerprint frequency";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 247);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(166, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Logharitm base for frequency bins";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 273);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(109, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "Frequency bins count";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 299);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(119, 13);
            this.label13.TabIndex = 12;
            this.label13.Text = "Fingerprint image length";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(3, 325);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(74, 13);
            this.label14.TabIndex = 13;
            this.label14.Text = "Top wavelets ";
            // 
            // btnReset
            // 
            this.btnReset.AutoSize = true;
            this.btnReset.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnReset.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnReset.Location = new System.Drawing.Point(3, 3);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(142, 23);
            this.btnReset.TabIndex = 14;
            this.btnReset.Text = "Restore defaults";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.AutoSize = true;
            this.tableLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.btnReset, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnSave, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(304, 354);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(296, 29);
            this.tableLayoutPanel2.TabIndex = 15;
            // 
            // btnSave
            // 
            this.btnSave.AutoSize = true;
            this.btnSave.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSave.Location = new System.Drawing.Point(151, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(142, 23);
            this.btnSave.TabIndex = 15;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtNumberOfHashtables
            // 
            this.txtNumberOfHashtables.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNumberOfHashtables.Location = new System.Drawing.Point(304, 16);
            this.txtNumberOfHashtables.Name = "txtNumberOfHashtables";
            this.txtNumberOfHashtables.Size = new System.Drawing.Size(296, 20);
            this.txtNumberOfHashtables.TabIndex = 16;
            // 
            // txtTrackDefinitionStepLength
            // 
            this.txtTrackDefinitionStepLength.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTrackDefinitionStepLength.Location = new System.Drawing.Point(304, 42);
            this.txtTrackDefinitionStepLength.Name = "txtTrackDefinitionStepLength";
            this.txtTrackDefinitionStepLength.Size = new System.Drawing.Size(296, 20);
            this.txtTrackDefinitionStepLength.TabIndex = 17;
            // 
            // txtTrackQueryMinStepLength
            // 
            this.txtTrackQueryMinStepLength.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTrackQueryMinStepLength.Location = new System.Drawing.Point(304, 68);
            this.txtTrackQueryMinStepLength.Name = "txtTrackQueryMinStepLength";
            this.txtTrackQueryMinStepLength.Size = new System.Drawing.Size(296, 20);
            this.txtTrackQueryMinStepLength.TabIndex = 18;
            // 
            // txtTrackQueryMaxStepLength
            // 
            this.txtTrackQueryMaxStepLength.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTrackQueryMaxStepLength.Location = new System.Drawing.Point(304, 94);
            this.txtTrackQueryMaxStepLength.Name = "txtTrackQueryMaxStepLength";
            this.txtTrackQueryMaxStepLength.Size = new System.Drawing.Size(296, 20);
            this.txtTrackQueryMaxStepLength.TabIndex = 19;
            // 
            // txtFingerprintSampleRate
            // 
            this.txtFingerprintSampleRate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtFingerprintSampleRate.Location = new System.Drawing.Point(304, 120);
            this.txtFingerprintSampleRate.Name = "txtFingerprintSampleRate";
            this.txtFingerprintSampleRate.Size = new System.Drawing.Size(296, 20);
            this.txtFingerprintSampleRate.TabIndex = 20;
            // 
            // txtOverlapOffsetSamples
            // 
            this.txtOverlapOffsetSamples.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtOverlapOffsetSamples.Location = new System.Drawing.Point(304, 146);
            this.txtOverlapOffsetSamples.Name = "txtOverlapOffsetSamples";
            this.txtOverlapOffsetSamples.Size = new System.Drawing.Size(296, 20);
            this.txtOverlapOffsetSamples.TabIndex = 21;
            // 
            // txtWindowSamplesSize
            // 
            this.txtWindowSamplesSize.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtWindowSamplesSize.Location = new System.Drawing.Point(304, 172);
            this.txtWindowSamplesSize.Name = "txtWindowSamplesSize";
            this.txtWindowSamplesSize.Size = new System.Drawing.Size(296, 20);
            this.txtWindowSamplesSize.TabIndex = 22;
            // 
            // txtMinFingerprintFrequency
            // 
            this.txtMinFingerprintFrequency.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMinFingerprintFrequency.Location = new System.Drawing.Point(304, 198);
            this.txtMinFingerprintFrequency.Name = "txtMinFingerprintFrequency";
            this.txtMinFingerprintFrequency.Size = new System.Drawing.Size(296, 20);
            this.txtMinFingerprintFrequency.TabIndex = 23;
            // 
            // txtMaxFingerprintFrequency
            // 
            this.txtMaxFingerprintFrequency.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMaxFingerprintFrequency.Location = new System.Drawing.Point(304, 224);
            this.txtMaxFingerprintFrequency.Name = "txtMaxFingerprintFrequency";
            this.txtMaxFingerprintFrequency.Size = new System.Drawing.Size(296, 20);
            this.txtMaxFingerprintFrequency.TabIndex = 24;
            // 
            // txtLogFrequenciesBase
            // 
            this.txtLogFrequenciesBase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLogFrequenciesBase.Location = new System.Drawing.Point(304, 250);
            this.txtLogFrequenciesBase.Name = "txtLogFrequenciesBase";
            this.txtLogFrequenciesBase.Size = new System.Drawing.Size(296, 20);
            this.txtLogFrequenciesBase.TabIndex = 25;
            // 
            // txtLogFrequenciesBins
            // 
            this.txtLogFrequenciesBins.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLogFrequenciesBins.Location = new System.Drawing.Point(304, 276);
            this.txtLogFrequenciesBins.Name = "txtLogFrequenciesBins";
            this.txtLogFrequenciesBins.Size = new System.Drawing.Size(296, 20);
            this.txtLogFrequenciesBins.TabIndex = 26;
            // 
            // txtFingerprintImageLength
            // 
            this.txtFingerprintImageLength.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtFingerprintImageLength.Location = new System.Drawing.Point(304, 302);
            this.txtFingerprintImageLength.Name = "txtFingerprintImageLength";
            this.txtFingerprintImageLength.Size = new System.Drawing.Size(296, 20);
            this.txtFingerprintImageLength.TabIndex = 27;
            // 
            // txtTopWavelets
            // 
            this.txtTopWavelets.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTopWavelets.Location = new System.Drawing.Point(304, 328);
            this.txtTopWavelets.Name = "txtTopWavelets";
            this.txtTopWavelets.Size = new System.Drawing.Size(296, 20);
            this.txtTopWavelets.TabIndex = 28;
            // 
            // SettingsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "SettingsControl";
            this.Size = new System.Drawing.Size(603, 385);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox txtNumberOfHashtables;
        private System.Windows.Forms.TextBox txtTrackDefinitionStepLength;
        private System.Windows.Forms.TextBox txtTrackQueryMinStepLength;
        private System.Windows.Forms.TextBox txtTrackQueryMaxStepLength;
        private System.Windows.Forms.TextBox txtFingerprintSampleRate;
        private System.Windows.Forms.TextBox txtOverlapOffsetSamples;
        private System.Windows.Forms.TextBox txtWindowSamplesSize;
        private System.Windows.Forms.TextBox txtMinFingerprintFrequency;
        private System.Windows.Forms.TextBox txtMaxFingerprintFrequency;
        private System.Windows.Forms.TextBox txtLogFrequenciesBase;
        private System.Windows.Forms.TextBox txtLogFrequenciesBins;
        private System.Windows.Forms.TextBox txtFingerprintImageLength;
        private System.Windows.Forms.TextBox txtTopWavelets;
    }
}
