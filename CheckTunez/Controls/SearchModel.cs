﻿using CheckTunez.Database;
using CheckTunez.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckTunez.Controls
{
    public class SearchModel
    {
        FingerprintTransactionScript _fingerPrintTransactionScript;

        public SearchModel()
        {
            var dbFacade = new SqliteFacade();
            dbFacade.OpenConnection();

            var configurationMapper = new ConfigurationMapper(new ConfigurationGateway(dbFacade));
            var trackMapper = new TrackMapper(new TrackGateway(dbFacade));
            var hashtablesMapper = new HashtablesMapper(new HashtablesGateway(dbFacade), trackMapper, configurationMapper);

            _fingerPrintTransactionScript = new FingerprintTransactionScript(configurationMapper, hashtablesMapper, trackMapper);
        }

        public List<Track> Search(string queryFilePath, Action<int> updateProgressDelegate)
        {
            var results = _fingerPrintTransactionScript.ExecuteSearch(queryFilePath, updateProgressDelegate);
            return results;
        }

    }
}
