﻿using CheckTunez.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CheckTunez.Controls
{
    public class SearchController
    {
        private SearchControl _control;
        private SearchModel _model;
        private BackgroundWorker _bgWorker;
        private List<Track> _resultTracks;

        public SearchController(SearchControl control, SearchModel model)
        {
            _control = control;
            _model = model;

            _bgWorker = new BackgroundWorker();
            _bgWorker.DoWork += bgWorker_DoWork;
            _bgWorker.ProgressChanged += bgWorker_ProgressChanged;
            _bgWorker.RunWorkerCompleted += bgWorker_RunWorkerCompleted;
            _bgWorker.WorkerSupportsCancellation = true;
            _bgWorker.WorkerReportsProgress = true;
        }

        private void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _control.FeedTableWithTracks(_resultTracks);
            _control.SetInputEnabled(true);
        }

        private void bgWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage == 0)
                _control.SetProgressLabel("Fingerprinting track sample");
            if (e.ProgressPercentage > 0)
                _control.SetProgressLabel("Searching in database");
            if (e.ProgressPercentage == 100)
                _control.SetProgressLabel("Done!");

            _control.SetProgressBar(e.ProgressPercentage);
        }

        private void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            Action<int> updateProgressDelegate = delegate(int progress) { _bgWorker.ReportProgress(progress); };
            var searchedFile = (string)e.Argument;
            _resultTracks = _model.Search(searchedFile, updateProgressDelegate);
        }

        public void ChooseFile()
        {
            using(var dialog = new OpenFileDialog())
            {
                dialog.Filter = "MPEG Layer-3 Audio files (*.mp3)|*.mp3";
                var result = dialog.ShowDialog();
                if (result == DialogResult.OK)
                    _control.UpdateFile(dialog.FileName);
            }
        }

        public void PerformSearch()
        {
            _control.SetInputEnabled(false);
            var searchedFile = _control.GetSearchedFile();

            List<Track> tracks;
            if(!File.Exists(searchedFile))
            {
                MessageBox.Show("Specified file doesn't exist!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                tracks = new List<Track>();
                _control.FeedTableWithTracks(tracks);
                _control.SetInputEnabled(true);
            }
            else
            {
                _bgWorker.RunWorkerAsync(searchedFile);
            }
        }

    }
}
