﻿using CheckTunez.Database;
using CheckTunez.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckTunez.Controls
{
    public class AddModel
    {
        private FingerprintTransactionScript _fingerPrintTransactionScript;
        
        public AddModel()
        {
            var dbFacade = new SqliteFacade();
            dbFacade.OpenConnection();

            var configurationMapper = new ConfigurationMapper(new ConfigurationGateway(dbFacade));
            var trackMapper = new TrackMapper(new TrackGateway(dbFacade));
            var hashtablesMapper = new HashtablesMapper(new HashtablesGateway(dbFacade), trackMapper, configurationMapper);

            _fingerPrintTransactionScript = new FingerprintTransactionScript(configurationMapper, hashtablesMapper, trackMapper);
        }

        public TrackToAdd PrepareSingleTrackToAdd(string path)
        {
            return new TrackToAdd()
            {
                Path = path,
                Added = false
            };
        }

        public List<TrackToAdd> PrepareFolderOFTracksToAdd(string path)
        {
            return Directory.EnumerateFiles(path, "*.mp3", SearchOption.AllDirectories)
                .Select(foundpath => new TrackToAdd() { Path = foundpath, Added = false }).ToList();
        }


        internal void AddTrackToDatabase(TrackToAdd track)
        {
            _fingerPrintTransactionScript.AddTrack(track.Path);
        }
    }
}
