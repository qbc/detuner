﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckTunez.Controls
{
    public class DatabaseController
    {
        private DatabaseControl _control;
        private DatabaseModel _model;

        public DatabaseController(DatabaseControl control, DatabaseModel model)
        {
            this._control = control;
            this._model = model;
        }

        public void Initialize()
        {
            var tracks = _model.GetTracks();
            _control.FeedTableWithTracks(tracks);

        }

        internal void UpdateRow(int rowNumber)
        {
            var id = _control.GetTrackIdForRow(rowNumber);
            var name = _control.GetTrackNameForRow(rowNumber);
            var author = _control.GetTrackAuthorForRow(rowNumber);

            _model.UpdateTrack(id, author, name);
        }

        internal void DeleteRow(int rowNumber)
        {
            var id = _control.GetTrackIdForRow(rowNumber);

            _model.DeleteTrack(id);
            
            _control.FeedTableWithTracks(_model.GetTracksFromCache());
        }
    }
}
