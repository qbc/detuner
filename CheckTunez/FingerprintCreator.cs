﻿using CheckTunez.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckTunez
{
    class FingerprintCreator
    {
        private PcmManager _pcmManager = new PcmManager();
        private HaarWavelet _waveletManager = new HaarWavelet();
        private SpectrumCreator _spectrumCreator = new SpectrumCreator();

        public List<bool[]> CreateFingerprintsForFile(string filename, IFingerprintingStep step, ConfigurationMapper config)
        {
            var samples = _pcmManager.GetMonoSamples(filename, config.FingerprintSampleRate);            
            _pcmManager.NormalizePcmTable(samples);

            var spectrogram = _spectrumCreator.CreateLogharitmicSpectrogram(samples, config);
            int spectrogramWidth = spectrogram.GetLength(0);

            var fpLength = config.FingerprintImageLength;
            var overlap = config.OverlapOffsetSamples;
            var topWavelets = config.TopWavelets;
            var logFrequenciesBins = config.LogFrequenciesBins;

            var fingerprints = new List<bool[]>();

            int fpStart = step.FirstStep / overlap;
            while (fpStart + fpLength < spectrogramWidth)
            {
                var frames = new float[fpLength][];
                for (int i = 0; i < fpLength; i++)
                    frames[i] = (float[])spectrogram[fpStart + i].Clone();

                _waveletManager.DecomposeHaar(frames);
                fingerprints.Add(_waveletManager.ExtractTopWavelets(frames, topWavelets));

                fpStart += fpLength + step.GetNextStep() / overlap;
            }
            return fingerprints;
        }
    }
}
