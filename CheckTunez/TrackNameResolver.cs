﻿using CheckTunez.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckTunez
{
    public class TrackNameResolver
    {
        public TrackName GetTrackName(string filename)
        {
            var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(filename);
            var indexOfPause = fileNameWithoutExtension.LastIndexOf('-');

            if(indexOfPause == - 1)
            {
                return new TrackName() { Artist = "Unknown artist", Title = fileNameWithoutExtension };                
            }

            return new TrackName() { Artist = fileNameWithoutExtension.Substring(0, indexOfPause), 
                Title = fileNameWithoutExtension.Substring(indexOfPause+1) };
        }
    }
}
