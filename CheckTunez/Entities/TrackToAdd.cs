﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckTunez.Entities
{
    public class TrackToAdd
    {
        public string Path { get; set; }
        public bool Added { get; set; }
    }
}
