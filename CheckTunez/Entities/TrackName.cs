﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckTunez.Entities
{
    public class TrackName
    {
        public string Artist { get; set; }
        public string Title { get; set; }
    }
}
