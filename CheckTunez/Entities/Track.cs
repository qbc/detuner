﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckTunez.Entities
{
    public class Track
    {
        public Track(int id)
        {
            Id = id;
        }

        public int Id { get; private set; }
        public string Artist { get; set; }
        public string Title { get; set; }
        public double Probability { get; set; }

    }
}
