﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckTunez.Entities
{
    public class Signature
    {
        public long[] Fingerprint { get; set; }
        Track track { get; set; }
    }
}
