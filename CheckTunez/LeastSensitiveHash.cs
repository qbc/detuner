﻿using CheckTunez.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckTunez
{
    class LeastSensitiveHash
    {
        public long[] GroupMinHashToLSHBuckets(int[] minHashes, int hashTablesNumber, int minHashesPerKey)
        {
            if (minHashesPerKey > sizeof(long))
                throw new ArgumentException("minHashesPerKey cannot be bigger than size of long");

            var LshBuckets = new long[hashTablesNumber];
            for (int i = 0; i < hashTablesNumber; i++)
            {
                var bytes = new byte[sizeof(long)];
                for (int j = 0; j < minHashesPerKey; j++)
                    bytes[j] = (byte)minHashes[i*minHashesPerKey+j];

                LshBuckets[i] = BitConverter.ToInt64(bytes, 0);
            }
            return LshBuckets;
        }
    }
}
